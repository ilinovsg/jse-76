package ru.ilinovsg.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;

import ru.ilinovsg.tm.dto.TaskDTO;
import ru.ilinovsg.tm.dto.TaskListResponseDTO;
import ru.ilinovsg.tm.dto.TaskResponseDTO;
import ru.ilinovsg.tm.service.TaskService;

@RestController
@RequestMapping("/task")
public class TaskController {

    private TaskService taskService;

    @Autowired
    public void setTaskService(TaskService taskService) {
        this.taskService = taskService;
    }

    @ApiOperation("Получаем задачу по id")
    @GetMapping("/{id}/")
    public TaskResponseDTO getTaskById(@PathVariable Long id) {
        return taskService.getTask(id);
    }

    @ApiOperation("Получаем список задач")
    @GetMapping(value = "/", produces = "application/json")
    public TaskListResponseDTO getAllTasks() {
        return taskService.getAllTasks();
    }

    @ApiOperation("Получаем задачи по названию")
    @GetMapping("/findByName")
    public TaskListResponseDTO getTaskByName(@RequestParam(required = false) String name) {
        return taskService.findByName(name);
    }

    @ApiOperation("Создание задачи")
    @PostMapping("/")
    public TaskResponseDTO createTask(@RequestBody TaskDTO taskDTO) {
        return taskService.createTask(taskDTO);
    }

    @ApiOperation("Удаление задачи")
    @DeleteMapping("/{id}/")
    public TaskResponseDTO deleteTask(@PathVariable Long id) {
        return taskService.deleteTask(id);
    }

    @ApiOperation("Обновление задачи")
    @PatchMapping("/")
    public TaskResponseDTO updateTask(@RequestBody TaskDTO taskDTO) {
        return taskService.updateTask(taskDTO);
    }
}

