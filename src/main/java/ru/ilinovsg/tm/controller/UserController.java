package ru.ilinovsg.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;

import ru.ilinovsg.tm.dto.UserDTO;
import ru.ilinovsg.tm.dto.UserListResponseDTO;
import ru.ilinovsg.tm.dto.UserResponseDTO;
import ru.ilinovsg.tm.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {
    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @ApiOperation("Получаем пользователя по id")
    @GetMapping("/{id}/")
    public UserResponseDTO getUserById(@PathVariable Long id) {
        return userService.getUser(id);
    }

    @ApiOperation("Получаем список пользователей")
    @GetMapping(value = "/", produces = "application/json")
    public UserListResponseDTO getAllUsers() {
        return userService.getAllUsers();
    }

    @ApiOperation("Получаем пользователей по логину")
    @GetMapping("/findByName")
    public UserListResponseDTO getUserByName(@RequestParam(required = false) String name) {
        return userService.findByName(name);
    }

    @ApiOperation("Создание пользователя")
    @PostMapping("/")
    public UserResponseDTO createUser(@RequestBody UserDTO userDTO) {
        return userService.createUser(userDTO);
    }

    @ApiOperation("Удаление пользователя")
    @DeleteMapping("/{id}/")
    public UserResponseDTO deleteUser(@PathVariable Long id) {
        return userService.deleteUser(id);
    }

    @ApiOperation("Обновление пользователя")
    @PatchMapping("/")
    public UserResponseDTO updateUser(@RequestBody UserDTO userDTO) {
        return userService.updateUser(userDTO);
    }
}
