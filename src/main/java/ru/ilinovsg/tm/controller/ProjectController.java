package ru.ilinovsg.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;

import ru.ilinovsg.tm.dto.ProjectDTO;
import ru.ilinovsg.tm.dto.ProjectListResponseDTO;
import ru.ilinovsg.tm.dto.ProjectResponseDTO;
import ru.ilinovsg.tm.service.ProjectService;

@RestController
@RequestMapping("/project")
public class ProjectController {
    private ProjectService projectService;

    @Autowired
    public void setProjectService(ProjectService projectService) {
        this.projectService = projectService;
    }

    @ApiOperation("Получаем проект по id")
    @GetMapping("/{id}/")
    public ProjectResponseDTO getProjectById(@PathVariable Long id) {
        return projectService.getProject(id);
    }

    @ApiOperation("Получаем список проектов")
    @GetMapping(value = "/", produces = "application/json")
    public ProjectListResponseDTO getAllProjects() {
        return projectService.getAllProjects();
    }

    @ApiOperation("Получаем проекты по названию")
    @GetMapping("/findByName")
    public ProjectListResponseDTO getProjectByName(@RequestParam(required = false) String name) {
        return projectService.findByName(name);
    }

    @ApiOperation("Создание проекта")
    @PostMapping("/")
    public ProjectResponseDTO createProject(@RequestBody ProjectDTO projectDTO) {
        return projectService.createProject(projectDTO);
    }

    @ApiOperation("Удаление проекта")
    @DeleteMapping("/{id}/")
    public ProjectResponseDTO deleteProject(@PathVariable Long id) {
        return projectService.deleteProject(id);
    }

    @ApiOperation("Обновление проекта")
    @PatchMapping("/")
    public ProjectResponseDTO updateProject(@RequestBody ProjectDTO projectDTO) {
        return projectService.updateProject(projectDTO);
    }
}
