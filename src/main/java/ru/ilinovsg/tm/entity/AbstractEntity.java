package ru.ilinovsg.tm.entity;

import javax.persistence.MappedSuperclass;

import java.io.Serializable;

@MappedSuperclass
public abstract class AbstractEntity implements Serializable {
}
