package ru.ilinovsg.tm.mapper;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import ru.ilinovsg.tm.dto.UserDTO;
import ru.ilinovsg.tm.dto.UserListResponseDTO;
import ru.ilinovsg.tm.entity.Users;
import ru.ilinovsg.tm.enumerated.Status;

public class UserMapper {
    public static UserDTO toDto(Users user) {
        return UserDTO.builder()
                .id(user.getId())
                .login(user.getLogin())
                .password(user.getPassword())
                .firstname(user.getFirstname())
                .lastname(user.getLastname())
                .build();
    }

    public static UserListResponseDTO toUserListResponseDto(Iterable<Users> users) {
        List<UserDTO>
                userList = StreamSupport.stream(users.spliterator(), false).map(UserMapper::toDto).collect(Collectors.toList());
        return UserListResponseDTO.builder().status(Status.OK).payload(userList).build();
    }
}
