package ru.ilinovsg.tm.mapper;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import ru.ilinovsg.tm.dto.ProjectDTO;
import ru.ilinovsg.tm.dto.ProjectListResponseDTO;
import ru.ilinovsg.tm.entity.Project;
import ru.ilinovsg.tm.enumerated.Status;

public class ProjectMapper {
    public static ProjectDTO toDto(Project project) {
        return ProjectDTO.builder()
                .id(project.getId())
                .name(project.getName())
                .description(project.getDescription())
                .userId(project.getUser().getId())
                .build();
    }

    public static ProjectListResponseDTO toProjectListResponseDto(Iterable<Project> projects) {
        List<ProjectDTO>
                projectList = StreamSupport.stream(projects.spliterator(), false).map(ProjectMapper::toDto).collect(Collectors.toList());
        return ProjectListResponseDTO.builder().status(Status.OK).payload(projectList).build();
    }

}
