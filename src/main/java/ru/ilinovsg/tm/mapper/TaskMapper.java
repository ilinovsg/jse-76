package ru.ilinovsg.tm.mapper;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import ru.ilinovsg.tm.dto.TaskDTO;
import ru.ilinovsg.tm.dto.TaskListResponseDTO;
import ru.ilinovsg.tm.entity.Task;
import ru.ilinovsg.tm.enumerated.Status;

public class TaskMapper {
    public static TaskDTO toDto(Task task) {
        return TaskDTO.builder()
                .id(task.getId())
                .name(task.getName())
                .description(task.getDescription())
                .projectId(task.getProject().getId())
                .userId(task.getUser().getId())
                .build();
    }

    public static TaskListResponseDTO toTaskListResponseDto(Iterable<Task> tasks) {
        List<TaskDTO>
                taskList = StreamSupport.stream(tasks.spliterator(), false).map(TaskMapper::toDto).collect(Collectors.toList());
        return TaskListResponseDTO.builder().status(Status.OK).payload(taskList).build();
    }
}
