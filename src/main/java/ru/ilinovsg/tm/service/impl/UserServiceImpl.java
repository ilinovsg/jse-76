package ru.ilinovsg.tm.service.impl;

import javax.transaction.Transactional;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import ru.ilinovsg.tm.dto.UserDTO;
import ru.ilinovsg.tm.dto.UserListResponseDTO;
import ru.ilinovsg.tm.dto.UserResponseDTO;
import ru.ilinovsg.tm.entity.Users;
import ru.ilinovsg.tm.enumerated.Status;
import ru.ilinovsg.tm.mapper.UserMapper;
import ru.ilinovsg.tm.repository.UserRepository;
import ru.ilinovsg.tm.service.UserService;

@Service
@Transactional
public class UserServiceImpl implements UserService {
    private UserRepository userRepository;

    //private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserResponseDTO createUser(UserDTO userDTO) {
        Users user = Users.builder()
                .login(userDTO.getLogin())
                //.password(passwordEncoder.encode(userDTO.getPassword()))
                .password(userDTO.getPassword())
                .firstname(userDTO.getFirstname())
                .lastname(userDTO.getLastname())
                .build();
        userRepository.save(user);
        return UserResponseDTO.builder().payload(UserMapper.toDto(user)).status(Status.OK).build();
    }

    @Override
    public UserResponseDTO updateUser(UserDTO userDTO) {
        Optional<Users> optionalUser = userRepository.findById(userDTO.getId());
        if (optionalUser.isPresent()) {
            Users user = optionalUser.get();
            user.setLogin(userDTO.getLogin());
            //user.setPassword(passwordEncoder.encode(userDTO.getPassword()));
            user.setPassword(userDTO.getPassword());
            user.setFirstname(userDTO.getFirstname());
            user.setLastname(userDTO.getLastname());
            return UserResponseDTO.builder().payload(UserMapper.toDto(user)).status(Status.OK).build();
        }
        return UserResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public UserResponseDTO deleteUser(Long id) {
        userRepository.deleteById(id);
        return UserResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public UserResponseDTO getUser(Long id) {
        Optional<Users> userOptional = userRepository.findById(id);
        if (userOptional.isPresent()) {
            return UserResponseDTO.builder().payload(UserMapper.toDto(userOptional.get())).status(Status.OK).build();
        }
        return UserResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public UserListResponseDTO getAllUsers() {
        Iterable<Users> users = userRepository.findAll();
        return UserMapper.toUserListResponseDto(users);
    }

    @Override
    public UserListResponseDTO findByName(String name) {
        List<Users> users = userRepository.findAllByLogin(name);
        return UserMapper.toUserListResponseDto(users);
    }
}
