package ru.ilinovsg.tm.service.impl;

import javax.transaction.Transactional;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.ilinovsg.tm.dto.ProjectDTO;
import ru.ilinovsg.tm.dto.ProjectListResponseDTO;
import ru.ilinovsg.tm.dto.ProjectResponseDTO;
import ru.ilinovsg.tm.entity.Project;
import ru.ilinovsg.tm.entity.Users;
import ru.ilinovsg.tm.enumerated.Status;
import ru.ilinovsg.tm.mapper.ProjectMapper;
import ru.ilinovsg.tm.repository.ProjectRepository;
import ru.ilinovsg.tm.repository.UserRepository;
import ru.ilinovsg.tm.service.ProjectService;

@Service
@Transactional
public class ProjectServiceImpl implements ProjectService {
    private ProjectRepository projectRepository;
    private UserRepository userRepository;

    @Autowired
    public void setProjectRepository(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public ProjectResponseDTO createProject(ProjectDTO projectDTO) {
        Optional<Users> optionalUser = userRepository.findById(projectDTO.getUserId());
        Users user = optionalUser.get();
        Project project = Project.builder()
                .name(projectDTO.getName())
                .description(projectDTO.getDescription())
                .user(user)
                .build();
        projectRepository.save(project);
        return ProjectResponseDTO.builder().payload(ProjectMapper.toDto(project)).status(Status.OK).build();
    }

    @Override
    public ProjectResponseDTO updateProject(ProjectDTO projectDTO) {
        Optional<Project> optionalProject = projectRepository.findById(projectDTO.getId());
        if (optionalProject.isPresent()) {
            Project project = optionalProject.get();
            project.setName(projectDTO.getName());
            project.setDescription(projectDTO.getDescription());
            Optional<Users> optionalUser = userRepository.findById(projectDTO.getUserId());
            Users user = optionalUser.get();
            project.setUser(user);
            return ProjectResponseDTO.builder().payload(ProjectMapper.toDto(project)).status(Status.OK).build();
        }
        return ProjectResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ProjectResponseDTO deleteProject(Long id) {
        projectRepository.deleteById(id);
        return ProjectResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ProjectResponseDTO getProject(Long id) {
        Optional<Project> projectOptional = projectRepository.findById(id);
        if (projectOptional.isPresent()) {
            return ProjectResponseDTO.builder().payload(ProjectMapper.toDto(projectOptional.get())).status(Status.OK).build();
        }
        return ProjectResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ProjectListResponseDTO getAllProjects() {
        Iterable<Project> projects = projectRepository.findAll();
        return ProjectMapper.toProjectListResponseDto(projects);
    }

    @Override
    public ProjectListResponseDTO findByName(String name) {
        List<Project> projects = projectRepository.findAllByName(name);
        return ProjectMapper.toProjectListResponseDto(projects);
    }
}
