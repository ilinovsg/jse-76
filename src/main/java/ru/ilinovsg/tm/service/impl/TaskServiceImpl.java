package ru.ilinovsg.tm.service.impl;

import javax.transaction.Transactional;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.ilinovsg.tm.dto.TaskDTO;
import ru.ilinovsg.tm.dto.TaskListResponseDTO;
import ru.ilinovsg.tm.dto.TaskResponseDTO;
import ru.ilinovsg.tm.entity.Project;
import ru.ilinovsg.tm.entity.Task;
import ru.ilinovsg.tm.entity.Users;
import ru.ilinovsg.tm.enumerated.Status;
import ru.ilinovsg.tm.mapper.TaskMapper;
import ru.ilinovsg.tm.repository.ProjectRepository;
import ru.ilinovsg.tm.repository.TaskRepository;
import ru.ilinovsg.tm.repository.UserRepository;
import ru.ilinovsg.tm.service.TaskService;

@Service
@Transactional
public class TaskServiceImpl implements TaskService {
    private TaskRepository taskRepository;
    private ProjectRepository projectRepository;
    private UserRepository userRepository;

    @Autowired
    public void setTaskRepository(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Autowired
    public void setProjectRepository(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public TaskResponseDTO createTask(TaskDTO taskDTO) {
        Optional<Project> optionalProject = projectRepository.findById(taskDTO.getProjectId());
        Project project = optionalProject.get();
        Optional<Users> optionalUser = userRepository.findById(taskDTO.getUserId());
        Users user = optionalUser.get();
        Task task = Task.builder()
                .name(taskDTO.getName())
                .description(taskDTO.getDescription())
                .project(project)
                .user(user)
                .build();
        taskRepository.save(task);
        return TaskResponseDTO.builder().payload(TaskMapper.toDto(task)).status(Status.OK).build();
    }

    @Override
    public TaskResponseDTO updateTask(TaskDTO taskDTO) {
        Optional<Task> optionalTask = taskRepository.findById(taskDTO.getId());
        if (optionalTask.isPresent()) {
            Task task = optionalTask.get();
            task.setName(taskDTO.getName());
            task.setDescription(taskDTO.getDescription());
            Optional<Project> optionalProject = projectRepository.findById(taskDTO.getProjectId());
            Project project = optionalProject.get();
            task.setProject(project);
            Optional<Users> optionalUser = userRepository.findById(taskDTO.getUserId());
            Users user = optionalUser.get();
            task.setUser(user);
            return TaskResponseDTO.builder().payload(TaskMapper.toDto(task)).status(Status.OK).build();
        }
        return TaskResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public TaskResponseDTO deleteTask(Long id) {
        taskRepository.deleteById(id);
        return TaskResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public TaskResponseDTO getTask(Long id) {
        Optional<Task> taskOptional = taskRepository.findById(id);
        if (taskOptional.isPresent()) {
            return TaskResponseDTO.builder().payload(TaskMapper.toDto(taskOptional.get())).status(Status.OK).build();
        }
        return TaskResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public TaskListResponseDTO getAllTasks() {
        Iterable<Task> tasks = taskRepository.findAll();
        return TaskMapper.toTaskListResponseDto(tasks);
    }

    @Override
    public TaskListResponseDTO findByName(String name) {
        List<Task> tasks = taskRepository.findAllByName(name);
        return TaskMapper.toTaskListResponseDto(tasks);
    }
}
