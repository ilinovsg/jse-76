package ru.ilinovsg.tm.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import ru.ilinovsg.tm.entity.Users;

public interface UserRepository extends CrudRepository<Users, Long> {
    List<Users> findAllByLogin(String login);
    Users findByLogin(String login);
}
