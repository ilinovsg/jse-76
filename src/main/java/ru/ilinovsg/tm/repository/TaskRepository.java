package ru.ilinovsg.tm.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import ru.ilinovsg.tm.entity.Task;

public interface TaskRepository extends CrudRepository<Task, Long> {
    List<Task> findAllByName(String name);
}
