package ru.ilinovsg.tm.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import ru.ilinovsg.tm.entity.Project;

public interface ProjectRepository extends CrudRepository<Project, Long> {
    List<Project> findAllByName(String name);
}
