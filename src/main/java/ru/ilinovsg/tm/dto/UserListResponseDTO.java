package ru.ilinovsg.tm.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import ru.ilinovsg.tm.enumerated.Status;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserListResponseDTO {
    private Status status;
    private List<UserDTO> payload;
}
