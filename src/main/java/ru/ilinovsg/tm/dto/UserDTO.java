package ru.ilinovsg.tm.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import ru.ilinovsg.tm.enumerated.Role;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO implements Serializable {
    public static final Long serialVersionUID = 1L;

    private Long id;
    private String login;
    private String password;
    private String firstname;
    private String lastname;
    private Role role;
}
